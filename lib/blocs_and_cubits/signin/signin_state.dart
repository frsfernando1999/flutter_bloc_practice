part of 'signin_cubit.dart';

enum SignInStatus {
  initial,
  submitting,
  success,
  error,
}
class SignInState extends Equatable{
  final SignInStatus signInStatus;
  final CustomError error;

  const SignInState({
    required this.signInStatus,
    required this.error,
  });

  factory SignInState.initial(){
    return const SignInState(signInStatus: SignInStatus.initial, error: const CustomError());
  }

  @override
  List<Object> get props => [signInStatus, error];

  @override
  String toString() {
    return 'SignInState{signInStatus: $signInStatus, error: $error}';
  }

  SignInState copyWith({
    SignInStatus? signInStatus,
    CustomError? error,
  }) {
    return SignInState(
      signInStatus: signInStatus ?? this.signInStatus,
      error: error ?? this.error,
    );
  }
}
